# QMDualToolbarNavigationViewController

[![Version](https://img.shields.io/cocoapods/v/QMDualToolbarNavigationViewController.svg?style=flat)](http://cocoadocs.org/docsets/QMDualToolbarNavigationViewController)
[![License](https://img.shields.io/cocoapods/l/QMDualToolbarNavigationViewController.svg?style=flat)](http://cocoadocs.org/docsets/QMDualToolbarNavigationViewController)
[![Platform](https://img.shields.io/cocoapods/p/QMDualToolbarNavigationViewController.svg?style=flat)](http://cocoadocs.org/docsets/QMDualToolbarNavigationViewController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QMDualToolbarNavigationViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "QMDualToolbarNavigationViewController"

## Author

Quico Moya, me@qmoya.com

## License

QMDualToolbarNavigationViewController is available under the MIT license. See the LICENSE file for more info.

