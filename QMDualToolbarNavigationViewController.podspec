Pod::Spec.new do |s|
  s.name             = "QMDualToolbarNavigationViewController"
  s.version          = '0.1.5'
  s.summary          = "A navigation controller with two toolbars."
  s.description      = <<-DESC
                       A simple subclass of UINavigationController that allows you to have,
                       in addition to the standard one, a toolbar under the navigation bar.
                       DESC
  s.homepage         = "http://qmoya.com"
  s.license          = 'MIT'
  s.author           = { "Quico Moya" => "me@qmoya.com" }
  s.source           = { :git => "https://github.com/qmoya/QMDualToolbarNavigationViewController.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/qmoya'

  s.ios.deployment_target = '7.0'
  s.requires_arc = true
  s.source_files = 'Classes'
  s.dependency 'BFNavigationBarDrawer', '~> 1.0'
end
