@import UIKit;

@interface QMDualToolbarNavigationViewController : UINavigationController <UIGestureRecognizerDelegate>

@property (nonatomic, weak) UIScrollView *scrollView;
@property (nonatomic, getter = isTopToolbarHidden) BOOL topToolbarHidden;
@property BOOL toolbarsFollowScrollView;
@property (nonatomic) NSArray *topToolbarItems;
@property CGFloat sensitivity;

- (void)setTopToolbarHidden:(BOOL)topToolbarHidden animated:(BOOL)animated;
- (void)setToolbarsHidden:(BOOL)toolbarsHidden animated:(BOOL)animated;
- (void)setTopToolbarItems:(NSArray *)items animated:(BOOL)animated;

@end
