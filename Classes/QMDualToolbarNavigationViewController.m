#import "QMDualToolbarNavigationViewController.h"
#import <BFNavigationBarDrawer.h>

typedef NS_ENUM (NSInteger, QMDualToolbarNavigationViewControllerTopToolbarState) {
	QMDualToolbarNavigationViewControllerTopToolbarStateHidden,
	QMDualToolbarNavigationViewControllerTopToolbarStateShowing,
	QMDualToolbarNavigationViewControllerTopToolbarStateShown,
	QMDualToolbarNavigationViewControllerTopToolbarStateHiding
};

@interface QMDualToolbarNavigationViewController ()

@property (nonatomic, readonly) QMDualToolbarNavigationViewControllerTopToolbarState topToolbarState;

@property CGFloat lastContentOffsetY;
@property BFNavigationBarDrawer *topToolbar;
@property BOOL topToolbarIsHiding;
@property BOOL topToolbarIsShowing;
@property UIPanGestureRecognizer *panGestureRecognizer;

@end

@implementation QMDualToolbarNavigationViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		self.topToolbar = [[BFNavigationBarDrawer alloc] init];
		self.sensitivity = 10.0f;
	}
	return self;
}

- (void)setTopToolbarHidden:(BOOL)topToolbarHidden animated:(BOOL)animated
{
	if (self.topToolbarState == QMDualToolbarNavigationViewControllerTopToolbarStateHiding || self.topToolbarState == QMDualToolbarNavigationViewControllerTopToolbarStateShowing) {
		return;
	}
	if (!topToolbarHidden && self.topToolbarState != QMDualToolbarNavigationViewControllerTopToolbarStateShown) {
		self.topToolbarIsShowing = YES;
		[self.topToolbar showFromNavigationBar:self.navigationBar animated:animated];
	} else if (topToolbarHidden && self.topToolbarState != QMDualToolbarNavigationViewControllerTopToolbarStateHidden) {
		self.topToolbarIsHiding = YES;
		[self.topToolbar hideAnimated:animated];
	}
}

- (void)setTopToolbarHidden:(BOOL)topToolbarHidden
{
	[self setTopToolbarHidden:topToolbarHidden animated:NO];
}

- (void)setToolbarsHidden:(BOOL)toolbarsHidden animated:(BOOL)animated
{
	[self setTopToolbarHidden:toolbarsHidden animated:animated];
	[self setToolbarHidden:toolbarsHidden animated:animated];
}

- (void)setScrollView:(UIScrollView *)scrollView
{
	_scrollView = scrollView;
	self.topToolbar.scrollView = scrollView;
	self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
	self.panGestureRecognizer.delegate = self;
	[self.scrollView addGestureRecognizer:self.panGestureRecognizer];
}

- (void)didScrollUp
{
	if (!self.toolbarsFollowScrollView) {
		return;
	}
	[self setTopToolbarHidden:YES animated:YES];
	[self setToolbarHidden:YES animated:YES];
}

- (void)didScrollDown
{
	if (!self.toolbarsFollowScrollView) {
		return;
	}
	[self setTopToolbarHidden:NO animated:YES];
	[self setToolbarHidden:NO animated:YES];
}

- (void)handlePan:(UIPanGestureRecognizer *)gesture
{
	if (!self.scrollView || gesture.view != self.scrollView) {
		return;
	}
	CGFloat contentOffsetY = self.scrollView.contentOffset.y;

	if (contentOffsetY < -self.scrollView.contentInset.top) {
		return;
	}
	if (gesture.state == UIGestureRecognizerStateBegan) {
		self.lastContentOffsetY = contentOffsetY;
		return;
	}
	CGFloat deltaY = contentOffsetY - self.lastContentOffsetY;
	if (deltaY < 0) {
		[self didScrollDown];
	} else if (deltaY > self.sensitivity) {
		[self didScrollUp];
	}
	self.lastContentOffsetY = contentOffsetY;
}

- (BOOL)				 gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
	shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

- (QMDualToolbarNavigationViewControllerTopToolbarState)topToolbarState
{
	if (self.topToolbarIsShowing) {
		if (self.topToolbar.isVisible) {
			self.topToolbarIsShowing = NO;
			return QMDualToolbarNavigationViewControllerTopToolbarStateShown;
		}
		return QMDualToolbarNavigationViewControllerTopToolbarStateShowing;
	}
	if (self.topToolbarIsHiding) {
		if (!self.topToolbar.isVisible) {
			self.topToolbarIsHiding = NO;
			return QMDualToolbarNavigationViewControllerTopToolbarStateHidden;
		}
		return QMDualToolbarNavigationViewControllerTopToolbarStateHiding;
	}
	if (self.topToolbar.isVisible) {
		return QMDualToolbarNavigationViewControllerTopToolbarStateShown;
	}
	return QMDualToolbarNavigationViewControllerTopToolbarStateHidden;
}

- (void)setTopToolbarItems:(NSArray *)items animated:(BOOL)animated
{
	[self.topToolbar setItems:items animated:animated];
}

- (void)setTopToolbarItems:(NSArray *)topToolbarItems
{
	[self setTopToolbarItems:topToolbarItems animated:NO];
}

- (NSArray *)topToolbarItems
{
	return self.topToolbar.items;
}

@end
